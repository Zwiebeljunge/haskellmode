# A function that expects a nix `pkgs`.  It's result is a derivation
# for a shell script that runs `node2nix` in the current directory
# with our `pkgs` set.
{ pkgs }:
pkgs.writeShellScriptBin "node2nix" ''
  rm -rf node_modules
  ${pkgs.lib.getExe pkgs.nodePackages.node2nix} -l package-lock.json \
          --output ./nix/node-registry.nix \
          --composition ./nix/node-default.nix \
          --node-env ./nix/node-env.nix 
''

