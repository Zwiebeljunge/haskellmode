{
  description = "my project description";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = inputs@{ self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        nodeDependencies = (pkgs.callPackage ./nix/node-default.nix {
          inherit pkgs;
          inherit system;
          inherit (pkgs) nodejs;
        }).nodeDependencies;
        version = import ./nix/version.nix;
      in {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [ vsce nodejs nodePackages.typescript ];
        };
        packages = {
          default = self.packages.${system}.haskellmode;
          haskellmode-version = version;
          haskellmode = pkgs.stdenv.mkDerivation {
            name = "haskellmode";
            src = ./.;
            buildInputs =
              [ pkgs.vsce pkgs.nodejs pkgs.nodePackages.typescript ];

            buildPhase = ''
              ln -snf ${nodeDependencies}/lib/node_modules ./node_modules

              vsce package

              mkdir $out

              mv haskellmode-${version}.vsix $out
            '';
          };

          callNode2nix = import ./nix/call-node2nix.nix { inherit pkgs; };
        };
      });
}
