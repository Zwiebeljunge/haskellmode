import { title } from 'process';
import * as vscode from 'vscode';
import * as cp from 'child_process';
import * as constants from './constants';
import * as path from 'path';
import * as fs from 'fs';

export function handleInsertImport(): void {
  const editor = vscode.window.activeTextEditor;

  if (!editor) {
    vscode.window.showInformationMessage("No editor active")
  }
  else {
    vscode.window.showInputBox({
      prompt: 'Enter module to import',
      validateInput: (text: string) => {
        if (constants.validModuleRegEx.test(text))
          return null;
        else
          return `Illegal namespace: ${text}`;
      }
    }).then((moduleToImport: string | undefined) => {
      if (moduleToImport != undefined) {
        vscode.window.showInputBox({
          prompt: 'Should the import be qualified? (y/n)'
        }).then((importQualified: string | undefined) => {
          if (importQualified == undefined) {
            vscode.window.showInformationMessage("Import canceled")
          }
          else if (importQualified == "n") {
            insertImport(editor, false, moduleToImport, undefined);
          } else {
            const importQualified = true;
            let defaultImportAlias: string = moduleToImport.split(".").slice(-1)[0];
            vscode.window.showInputBox({
              value: defaultImportAlias,
              prompt: 'How would you like to qualify the import?',
              validateInput: (text: string) => {
                if (constants.validImportAliasRegEx.test(text))
                  return null;
                else
                  return `Illegal qualification: ${text}`;
              }
            }).then((qualification: string | undefined) => {
              if (qualification === "" || qualification == moduleToImport || qualification == undefined) {
                insertImport(editor, importQualified, moduleToImport, undefined);
              }
              else {
                insertImport(editor, importQualified, moduleToImport, qualification);
              }
            })
          }
        })
      }
    });
  }
}

function insertImport(editor: vscode.TextEditor, qualified: boolean, module: string, qualification?: string) {
  let document = editor.document;
  // Get the document text
  const documentText = document.getText();

  editor.edit(edit => {
    const numberLines = document.lineCount;
    var importStartLine = 0;
    var importStopLine = 0;
    var isFirstImportLine = true;

    for (let lineNumber = 0; lineNumber < numberLines; lineNumber++) {
      var line = document.lineAt(lineNumber).text;
      if (line.startsWith('module')) {
        importStartLine = lineNumber + 1;
        importStopLine = lineNumber + 1;
      }
      if (line.startsWith('import')) {
        if (isFirstImportLine) {
          importStartLine = lineNumber;
          isFirstImportLine = false;
        }
        importStopLine = lineNumber;
      } else if (!isFirstImportLine) {
        break;
      }
    }
    var importLine = "";
    if (qualified) {
      if (typeof qualification !== 'undefined') {
        importLine = `import qualified ${module} as ${qualification}\n`
      }
      else {
        importLine = `import qualified ${module}\n`
      }
    } else {
      importLine = `import ${module}\n`
    }

    edit.insert(new vscode.Position(importStartLine, 0), importLine)

    const importRange = new vscode.Range(new vscode.Position(importStartLine, 0), new vscode.Position(importStopLine + 1, 0));
    vscode.commands.executeCommand('vscode.executeFormatRangeProvider', editor.document.uri, importRange)
  });
}

export function handleInsertPragma(): void {
  const editor = vscode.window.activeTextEditor;

  if (!editor) vscode.window.showInformationMessage("No editor active")
  else {
    cp.exec('ghc --supported-extensions', (err, stdout, _) => {
      var pragmas: string[] = constants.languagePragmas;

      if (!err) {
        pragmas = stdout.split("\n");
      }
      vscode.window.showQuickPick(pragmas).then((selectedPragma: string | undefined) => {
        if (selectedPragma != undefined)
          insertPragma(editor, "LANGUAGE", selectedPragma);
      });
    });
  }
}

function mkPragma(pragmaType: string, pragmaValue: string): string {
  return `{-# ${pragmaType} ${pragmaValue} #-}\n`;
}

function insertPragma(editor: vscode.TextEditor, pragmaType: string, pragmaValue: string) {
  const pragmaLine: string = mkPragma(pragmaType, pragmaValue);
  editor.edit(edit => {
    edit.insert(new vscode.Position(0, 0), pragmaLine)
  });
}

export function handleInsertOptionsGHC(): void {
  const editor = vscode.window.activeTextEditor;

  if (!editor) vscode.window.showInformationMessage("No editor active")
  else {
    cp.exec('ghc --show-options', (err, stdout, _) => {
      var pragmas: string[] = [];

      if (!err) {
        pragmas = stdout.split("\n").filter((pragma: string) => !pragma.startsWith("-X"));
      }
      vscode.window.showQuickPick(pragmas).then((selectedPragma: string | undefined) => {
        if (selectedPragma != undefined)
          insertPragma(editor, "OPTIONS_GHC", selectedPragma);
      });
    });
  }
}

function searchForCabalFile(workspaceDirectory: string, currentPath: string): string | undefined {
  const cabalFile = fs.readdirSync(currentPath).find(file => file.toLowerCase().endsWith('.cabal'));

  if (cabalFile) {
    return path.join(currentPath, cabalFile);
  }

  if (currentPath === workspaceDirectory) {
    return undefined;
  }

  return searchForCabalFile(workspaceDirectory, path.dirname(currentPath));
}

export function handleInsertDependency(): void {
  const editor = vscode.window.activeTextEditor;

  if (!editor) vscode.window.showInformationMessage("No editor active")
  else {
    let currentDirectory = path.dirname(editor.document.uri.fsPath);
    let workspaceDirectory = vscode.workspace.rootPath;

    if (workspaceDirectory === undefined)
      vscode.window.showInformationMessage("No workspace selected");
    else {
      let cabalFilePath = searchForCabalFile(workspaceDirectory, currentDirectory);

      if (cabalFilePath === undefined)
        vscode.window.showInformationMessage("Could not find cabal file in workspace.");
      else {
        vscode.window.showInputBox({
          prompt: 'Enter module to import',
          validateInput: (text: string) => {
            if (text != "")
              return null;
            else
              return `Illegal namespace: ${text}`;
          }
        }).then((dependency: string | undefined) => {
          if (dependency !== undefined) {
            const config = vscode.workspace.getConfiguration('haskellmode');
            const cabalAddPath = config.get<string>('cabalAddPath');

            let cmd: string = cabalAddPath + " -f " + cabalFilePath + " " + dependency;

            cp.exec(cmd, (err, stdout, _) => {
              vscode.window.showInformationMessage(stdout);
              if (err)
                throw err;
            });
          }
        });
      }
    }
  }
}