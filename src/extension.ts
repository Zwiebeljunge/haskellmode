// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as func from './functionality';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	let addImport = vscode.commands.registerCommand('haskellmode.import', func.handleInsertImport);
	context.subscriptions.push(addImport);

	let addLanguagePragma = vscode.commands.registerCommand('haskellmode.extension', func.handleInsertPragma);
	context.subscriptions.push(addLanguagePragma);

	let addOptionsGHC = vscode.commands.registerCommand('haskellmode.optionsGHC', func.handleInsertOptionsGHC);
	context.subscriptions.push(addOptionsGHC);

	let addDependency = vscode.commands.registerCommand('haskellmode.dependency', func.handleInsertDependency);
	context.subscriptions.push(addDependency);
}

// this method is called when your extension is deactivated
export function deactivate() { }
