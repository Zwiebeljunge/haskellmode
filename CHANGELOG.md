# Change Log

All notable changes to the "haskellmode" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## Version 0.0.1

- Insertion of import statements
- Insertion of language pragmas

## Version 0.0.2

- Default qualification on import

## Version 0.0.3

- Uses ghc to get list of available pragmas

## Version 0.0.4

- Insertion of pragmas OPTION_GHC

## Version 0.0.5

- Insertion of dependency in .cabal file

## Version 0.0.7

- Make path of cabal-add configurable via settings.
