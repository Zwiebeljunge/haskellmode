# haskellmode

This an extension which provides a haskell mode for vscode.

## Features
- Add (qualified) modules imports
- Add pragmas (LANGUAGE & OPTION_GHC)
- Add dependency to cabal file

## TODO's
  - when inserting pragma `-fplugin` ask for plugin name
  - setting of breakpoints via `Debug.Breakpoint` plugin
  - infer component to add dependency in cabal file from current file
